package com.saskpolytech.cst119.soundeffects;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.File;
import java.util.ArrayList;

/**
 * Purpose: To provide the main UI for the app, which will list all sound
 *          effects in the database and will give the user the ability to
 *          play, edit, delete, and add sound effects, either from within
 *          this activity or within the EditActivity.
 *
 * @author Corey Janzen
 * @version 1.00
 * @created May 21, 2017
 */
public class MainActivity extends AppCompatActivity implements AdapterView
        .OnItemClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        MediaPlayer.OnCompletionListener
{

    /* CONSTANTS */
    /** The Extra for the SoundEffect */
    public static final String SFX = "sfx";
    /** The Extra for determining if this is an update to a sound effect or a new sound effect */
    public static final String NEW = "new";
    /** The Request Code for the edit menu */
    public static final int EDIT_CODE = 0;
    /** The Request Code for the new menu */
    public static final int NEW_CODE = 1;
    /** Request code for Checking Permissions */
    public static final int CHECK_STORAGE_PERMS = 2;
    /** The ID for the missing file notification */
    public static final int MISS_FILE_ID = 100;
    // The interval for updating the location
    private static final int INTERVAL = 30000;
    // The fastest interval for updating the location
    private static final int FAST_INTERVAL = 15000;

    /* ATTRIBUTES */
    // The action bar of the main UI
    private Toolbar actionBar;

    // ListView-related resources
    private ListView lstSounds;
    private ArrayList<SoundEffect> sounds;
    private SoundListAdapter sfxAdapter;

    // The database
    private SoundEffectDatabase db;

    // The media player used to play sound effects
    private MediaPlayer player;

    // Client for connecting to GMS
    private GoogleApiClient mGoogleApiClient;

    // The current location of the device
    private double latitude;
    private double longitude;

    // The current filter being used
    private String filter;


    /* BEHAVIOURS */


    @Override
    /**********************************************************************
     * Purpose: The onCreate method for this class. Used on the creation of
     *          the activity. Not intended to be used as a regular method.
     * @param savedInstanceState (Bundle): The bundle that was passed from
     *                                     another activity.
     */
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Initialize the database
        db = new SoundEffectDatabase(this, new DefaultSounds(this));

        // Create the ActionBar
        createActionBar();
        // Create the list
        createSoundsList();
        // Set the filter variable up
        filter = "";
        // Set location to be 0 by default
        latitude = 0;
        longitude = 0;

        // Setup the GMS
        setupGMS();

    }


    /**********************************************************************
     * Purpose: To create the action bar for the MainActivity.
     */
    private void createActionBar()
    {
        // Find the toolbar in the activity_main layout
        actionBar = (Toolbar) findViewById(R.id.mainToolbar);
        // Dynamically configure the toolbar
        actionBar.setTitle(getString(R.string.main_action_bar_title));
        setSupportActionBar(actionBar);
    }


    /**********************************************************************
     * Purpose: To create the options for the action bar for this activity.
     *          Not intended to be used as a regular method.
     * @param menu (Menu): The menu to be inflated.
     * @return A boolean value determining if the menu was successfully
     *         created.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Get a Menu Inflater
        MenuInflater inflater = getMenuInflater();
        // Inflate the menu with the Menu Inflater
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }


    /**********************************************************************
     * Purpose: To handle events where a user clicks on one of the action
     *          bar items in the main activity. Not intended to be used
     *          as a regular method.
     * @param item (MenuItem): The item that was clicked on.
     * @return A boolean value determining if item was handled successfully.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            // If the New button was pressed
            case R.id.itNewSound:
                // Handle functionality in another method
                handleNew();
                return true;
            // If Filter By was pressed
            case R.id.itFilter:
                // Do nothing
                return true;
            // If the Delete All button was pressed
            case R.id.itClear:
                // Clear the database in another method
                handleClear();
                return true;
            // If this button hasn't been defined
            default:
                // Check if one of the filter buttons were pressed
                return handleFilter( item );
        }
    }


    /**********************************************************************
     * Purpose: To create the list of sound effects for the ListView of this
     *          activity.
     */
    private void createSoundsList()
    {
        // Find the ListView in the activity_main layout
        lstSounds = (ListView) findViewById(R.id.lstSounds);
        // Give it a listener
        lstSounds.setOnItemClickListener(this);
        // Dynamically configure the list view with a list of sound effects
        //  from the database
        updateSoundsList();
    }


    /**********************************************************************
     * Purpose: To update the list of sound effects for the ListView of this
     *          activity.
     */
    private void updateSoundsList()
    {
        // Get the list of sound effects from the database
        sounds = new ArrayList<SoundEffect>();
        db.open();
        sounds.addAll(db.getAllSoundEffects());
        db.close();

        // Check for any sound effects with a missing file
        checkSoundEffects(sounds);

        // Add the list to the ListView via Adapter
        sfxAdapter = new SoundListAdapter(this, sounds, this);
        lstSounds.setAdapter(sfxAdapter);

        // Set the filter to nothing
        filter = "";
    }


    /**********************************************************************
     * Purpose: Handles events where the user clicks on an item in the list.
     *          Not intended to be used as a regular method.
     *          NOTE: This method is no longer being used. handleButtons()
     *          is being used instead to handle this event.
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id)
    {
        // Get the Sound Effect object
        SoundEffect sfx = sfxAdapter.getItem(position);

        // Just display a Toast for now
        General.printToast(this, "Sound ID: " + sfx.id);
    }


    /**********************************************************************
     * Purpose: Handles events where the user clicks on an item in the list.
     *          Intended to be used in the SoundListAdapter class so that
     *          events regarding to clicking on buttons from a list item
     *          could be handled in the MainActivity.
     * @param sfx (SoundEffect): The SoundEffect related to the item that was
     *                           clicked on.
     * @param v (View): The View that was clicked on.
     */
    public void handleButtons( SoundEffect sfx, View v )
    {
        switch (v.getId())
        {
            // If the user clicks on the item itself
            case R.id.glSoundItem:
                // handle the Item in a separate method
                handleListItem(sfx, v);
                break;
            // If the user clicks on the edit button
            case R.id.btnEdit:
                // handle the Edit button in a separate method
                handleEdit(sfx);
                break;
            // If the user clicks on the edit button
            case R.id.btnDelete:
                // handle the Delete button in a separate method
                handleDelete(sfx);
                break;
        }
    }


    /**********************************************************************
     * Purpose: Handles an event where the list item itself was clicked on,
     *          but neither of the buttons in the list item were clicked.
     *          Should play a sound effect.
     * @param sfx (SoundEffect): The SoundEffect related to the item that was
     *                           clicked on.
     * @param v (View): The View that was clicked on.
     */
    private void handleListItem( SoundEffect sfx, View v )
    {
        // Display an animation
        v.startAnimation(AnimationUtils.loadAnimation(this,
                android.R.anim.fade_in));
        // Play Sound Effect
        playSound(sfx);
        // Update the sfx's location
        updateLocation(sfx, v);
    }


    /**********************************************************************
     * Purpose: Handles an event where the "Edit" button of a list item was
     *          clicked on. Should take you to the EditActivity to edit
     *          the sound effect.
     * @param sfx (SoundEffect): The SoundEffect related to the item that was
     *                           clicked on.
     */
    private void handleEdit( SoundEffect sfx )
    {
        // Setup an Intent
        Intent intent = new Intent(this, EditActivity.class);
        // Pass the SoundEffect object
        intent.putExtra(SFX, sfx);
        // Start the activity and wait for the result
        this.startActivityForResult(intent, EDIT_CODE);
    }


    /**********************************************************************
     * Purpose: Handles an event where the "New" button of the action bar was
     *          pressed. Should take you to the EditActivity to make the new
     *          sound effect.
     */
    private void handleNew( )
    {
        // Setup an Intent
        Intent intent = new Intent(this, EditActivity.class);
        // Pass a null SoundEffect object to create a new Sound Effect
        intent.putExtra(SFX, (SoundEffect) null);
        // Start the activity and wait for the result
        this.startActivityForResult(intent, NEW_CODE);
    }


    /**********************************************************************
     * Purpose: Handles an event where the EditActivity returns something
     *          to this activity. That something is usually a SoundEffect.
     *          However, there is an event where the EditActivity doesn't
     *          return something because the user canceled the edit/addition
     *          of a sound effect. In this case, the method does nothing.
     * @param requestCode (int): The code that represents the id of the
     *                           request (is a new sound effect being
     *                           requested, or is a sound effect being
     *                           edited?)
     * @param resultCode (int): The code that represents the result of a
     *                          request. Used to determine if a request was
     *                          canceled.
     * @param data (Intent): The data being returned from the EditActivity.
     *                       Usually a SoundEffect object.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data)
    {
        // Grab the result from the intent
        Bundle bundle = data.getExtras();

        // If we didn't cancel the request, then check what code we got back
        if ( resultCode != RESULT_CANCELED )
        {
            switch (requestCode)
            {
                // If the Edit Activity returned something that was edited
                case EDIT_CODE:
                    // Update the sound effect
                    updateSoundEffect( (SoundEffect) bundle.get(SFX) );
                    break;
                // If the Edit Activity returned something that is new
                case NEW_CODE:
                    // Add the sound effect
                    createSoundEffect( (SoundEffect) bundle.get(SFX) );
                    break;
            }
            // Refresh the list
            updateSoundsList();
        }
    }


    /**********************************************************************
     * Purpose: Handles an event where a sound effect has been edited. In
     *          this case, save the changes to the database.
     * @param sfx (SoundEffect): The sound effect that was edited.
     */
    private void updateSoundEffect( SoundEffect sfx )
    {
        // Pass the responsibility to the database
        db.open();
        db.updateSoundEffect(sfx);
        db.close();
    }


    /**********************************************************************
     * Purpose: Handles an event where a new sound effect has been made. In
     *          this case, save the sound effect to the database.
     * @param sfx (SoundEffect): The sound effect that was made.
     */
    private void createSoundEffect( SoundEffect sfx )
    {
        // Pass the responsibility to the database
        db.open();
        db.createSoundEffect(sfx);
        db.close();
    }


    /**********************************************************************
     * Purpose: Checks all of the sound effects from the specified list to
     *          see if any are missing files. If there is a missing sound
     *          effect, notify the user via notification.
     * @param sfxs (ArrayList[SoundEffect]): The list of sound effects to
     *                                       check for.
     */
    private void checkSoundEffects( ArrayList<SoundEffect> sfxs )
    {
        // Open database
        db.open();
        // Go through each sound effects
        for ( SoundEffect s : sfxs )
        {
            // Grab a reference to the file location
            File file = new File(SoundEffect.DIRECTORY, s.file);
            // If this file doesn't exist
            if ( !file.exists() )
            {
                // Throw a notification warning the user that this sound
                //  effect doesn't exist
                notifyUserOfMissingFile(s);
            }
        }
    }


    /**********************************************************************
     * Purpose: A helper method for checkSoundEffects(). Notifies the user
     *          that a sound effect is missing its file.
     * @param sfx (SoundEffect): The sound effect that's missing its file.
     */
    private void notifyUserOfMissingFile( SoundEffect sfx )
    {
        // Setup the notification and the builder
        NotificationManager notifyMgr = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        // Set the icon for the notification
        builder.setSmallIcon(R.mipmap.ic_launcher);
        // Set the ticker message
        builder.setTicker(getString(R.string.miss_file_ticker));
        // Set the title
        builder.setContentTitle(getString(R.string.miss_file_title));
        // Set the message
        builder.setContentText(getString(R.string.miss_file_msg) + " " +
                sfx.name);
        // Ensure this notification pops up right aways
        builder.setWhen(System.currentTimeMillis());

        // Build the notification using the builder and show the notification
        Notification n = builder.build();
        notifyMgr.notify(MISS_FILE_ID, n);

    }


    /**********************************************************************
     * Purpose: Handles an event where the "Delete" button of a list item was
     *          clicked on. Should delete the sound effect from both the list
     *          and the database.
     * @param sfx (SoundEffect): The SoundEffect related to the item that was
     *                           clicked on.
     */
    private void handleDelete( SoundEffect sfx )
    {
        // Let the database handle the deleting
        db.open();
        db.deleteSoundEffect(sfx);
        db.close();
        // Update the SoundEffects list in the main activity
        if (filter.equals(""))
        {
            updateSoundsList();
        }
        else
        {
            updateSoundsListWithFilter(filter);
        }

    }


    /**********************************************************************
     * Purpose: Handles an event where the "Delete All" button of the action
     *          bar was clicked on. Should delete all sound effects from both
     *          the list and the database.
     */
    private void handleClear()
    {
        // Let the database clear itself out
        db.open();
        db.clear();
        db.close();
        // Update the SoundEffects list in the main activity
        updateSoundsList();
    }


    /**********************************************************************
     * Purpose: Handles an event where one of the filter buttons from the
     *          action bar are clicked on. Should filter sound effects
     *          by whatever filter the user chose.
     * @param item (MenuItem): The menu item that was clicked on
     * @return A boolean value that determines whether the filter buttons
     *         were handled successfully or not.
     */
    private boolean handleFilter( MenuItem item )
    {
        // Determine which filter button was pressed
        switch (item.getItemId())
        {
            // If the clear filter button was pressed
            case R.id.itClearFilter:
                // Update sound list with all sound effects
                updateSoundsList();
                break;
            // If any other filters were pressed
            default:
                // if this item has text
                if ( !item.getTitle().toString().equals("") )
                {
                    // Pass the string of the item as the type to
                    //  filter by.
                    updateSoundsListWithFilter(item.getTitle().toString());
                    return true;
                }

        }

        // Let the super class handle the item
        return super.onOptionsItemSelected(item);
    }


    /**********************************************************************
     * Purpose: To filter the ListView of sound effects for this activity
     *          so that only the specified type of sound effects are
     *          displayed.
     * @param type (String): The type that you want to filter by.
     */
    private void updateSoundsListWithFilter( String type )
    {
        // Get the list of sound effects from the database with
        //  the specified type
        sounds = new ArrayList<SoundEffect>();
        db.open();
        sounds.addAll(db.getSoundEffectsByFilter(type));
        db.close();

        // Check for any sound effects with a missing file
        checkSoundEffects(sounds);

        // Add the list to the ListView via Adapter
        sfxAdapter = new SoundListAdapter(this, sounds, this);
        lstSounds.setAdapter(sfxAdapter);

        filter = type;
    }


    /**********************************************************************
     * Purpose: Plays the specified sound effect for the user, if the
     *          specified sound effect exists. Otherwise, display an error
     *          message telling the user that the specified sound effect
     *          is missing its file.
     * @param sfx (SoundEffect): The SoundEffect to play
     */
    private void playSound( SoundEffect sfx )
    {
        // If there is a sound effect already playing
        if ( player != null && player.isPlaying() )
        {
            // Stop the sound
            player.stop();
        }
        // Release the sound
        releasePlayer();

        // Grab the media player for the sound effect
        player = sfx.makeMediaPlayer(this);
        // Play the sound if it exists
        if ( player != null )
        {
            player.start();
        }
        else
        {
            // Pop up error message
            General.printToast(this, getString(R.string.play_failed));
        }
    }


    /* Location Services Behaviours */


    /**********************************************************************
     * Purpose: To start up Location Services whenever the app is started.
     */
    @Override
    protected void onStart()
    {
        // Start the Location Services
        mGoogleApiClient.connect();
        super.onStart();
    }


    /**********************************************************************
     * Purpose: To stop Location Services whenever the app is stopped to save
     *          resources.
     */
    @Override
    protected void onStop()
    {
        // Stop the Location Services
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    @Override
    /**********************************************************************
     * Purpose: To handle the event when Location Services is connected
     *          and enabled for use
     * @param bundle (Bundle): An optional bundle of data from another intent.
     */
    public void onConnected(@Nullable Bundle bundle)
    {
        // Successful connection to GooglePlay GMS
        try
        {
            //  try to get the last location
            Location lastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
            // Check if that worked
            if ( lastLocation != null )
            {
                // If that worked, set the current location to last location.
                latitude = lastLocation.getLatitude();
                longitude = lastLocation.getLongitude();
            }

            // Setup a request to listen for changes to location
            setupLocationRequests();
        }
        catch (SecurityException e)
        {
            // Do nothing, it doesn't matter if we don't have last location
        }
    }

    /**********************************************************************
     * Purpose: To handle the event when Location Services gets suspended
     * @param i (int): The cause of the suspension.
     */
    @Override
    public void onConnectionSuspended(int i)
    {
        // Print a toast indicating that the GMS service has been
        //  suspended
        General.printToast(this, getString(R.string.connect_suspend));
    }


    /**********************************************************************
     * Purpose: To handle the event when Location Services fails to
     *          connect
     * @param connectionResult (ConnectionResult):
     *            Represents the reason for the failure.
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult)
    {
        // Print a toast indicating that the GMS service has failed
        General.printToast(this, getString(R.string.connect_failed) +
                connectionResult.getErrorCode());
    }

    /**********************************************************************
     * Purpose: To handle the event when phone's location has been changed.
     *          Should update the current location variables for this
     *          activity.
     * @param location (Lcation): The new location of the phone.
     */
    @Override
    public void onLocationChanged(Location location)
    {
        // Update the latitude and longitude variables
        latitude = location.getLatitude();
        longitude =  location.getLongitude();
    }


    /**********************************************************************
     * Purpose: This is a helper method for onConnected() which sets up
     *          Location Requests at a certain interval.
     * @throws SecurityException
     */
    private void setupLocationRequests() throws SecurityException
    {
        // Setup the Location Request
        LocationRequest locationRequest = new LocationRequest();
        // Set intervals
        locationRequest.setInterval(INTERVAL);
        locationRequest.setFastestInterval(FAST_INTERVAL);
        // Set the request's priority to high accuracy
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Sound the request over to the API
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                locationRequest, this);
    }


    /**********************************************************************
     * Purpose: To update the location of a sound in both the sounds list
     *          view and the database. Used in the handleListItem() method
     *          whenever a user plays a sound effect.
     * @param sfx (SoundEffect): The sound effect to update in the database
     * @param v (View): The view that was clicked on
     */
    private void updateLocation( SoundEffect sfx, View v )
    {
        // The latitude, longitude TextViews
        TextView tvSLat = (TextView) v.findViewById( R.id.tvSLat );
        TextView tvSLon = (TextView) v.findViewById( R.id.tvSLon );

        // Update the location in both the list and the database
        sfx.latitude = this.latitude;
        sfx.longitude = this.longitude;
        db.open();
        db.updateSoundEffect(sfx);
        db.close();

        tvSLat.setText(SoundListAdapter.LATITUDE + this.latitude);
        tvSLon.setText(SoundListAdapter.LONGITUDE + this.longitude);

    }


    /**********************************************************************
     * Purpose: To set up GMS services in order to use location services.
     */
    private void setupGMS()
    {
        if ( mGoogleApiClient == null )
        {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }


    /* End of Location Services Behaviours */


    /**********************************************************************
     * Purpose: The method that will be called whenever this activity is
     *          destroyed. It should release the media player object that's
     *          currently in use.
     */
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        // Release resources from the media player if that hasn't
        //  been done already
        releasePlayer();
    }


    /**********************************************************************
     * Purpose: To release the media player object that's currently in use
     *          to save on resources.
     */
    private void releasePlayer()
    {
        // Release resources from the media player if that hasn't
        //  been done already
        if ( player != null )
        {
            player.release();
        }
    }


    /**********************************************************************
     * Purpose: To handle the event when the MediaPlayer finishes playing its
     *          sound effect. Should release the media player object.
     */
    @Override
    public void onCompletion(MediaPlayer mp)
    {
        // Release the player
        releasePlayer();
    }
}
