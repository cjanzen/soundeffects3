package com.saskpolytech.cst119.soundeffects;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Purpose: To provide an interface for the user to edit properties for
 *          sound effects in the database. Also used to make new sound
 *          effects with user-specified properties. The class communicates
 *          with the MainActivity class to pass back values for the edited
 *          or new sound effect.
 *
 * @author Corey Janzen
 * @version 1.00
 * @created May 28, 2017
 */
public class EditActivity extends AppCompatActivity implements View
        .OnClickListener
{


    // Various elements in the Edit Activity
    private Toolbar actionBar;
    private EditText etName;
    private EditText etDesc;
    private Spinner spnType;
    private TextView tvCurrentTime;
    private TextView tvFilename;
    private Button btnFile;

    // A list of files for the file chooser dialog
    private String[] fileList;
    // The directory for the sound effect files
    private File mPath;
    // The string representing the chosen file
    private String choosenFile;
    // The type of file to look for
    private static final String FTYPE = ".wav";

    // The SoundEffect used by this activity;
    private SoundEffect sfx;


    /**********************************************************************
     * Purpose: The onCreate method for this class. Used on the creation of
     *          the activity. Not intended to be used as a regular method.
     * @param savedInstanceState (Bundle): The bundle that was passed from
     *                                     the MainActivity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        // Setup the elements for this activity
        createActionBar();
        createElements();
        setupElements();

        // Find the sound effects directory
        mPath = SoundEffect.DIRECTORY;
    }


    /**********************************************************************
     * Purpose: To create references to various elements on the EditActivity
     *          layout.
     */
    private void createElements()
    {
        // Initialize the variables holding references to the views in the
        //  EditActivity
        etName = (EditText) findViewById(R.id.etName);
        etDesc = (EditText) findViewById(R.id.etDesc);
        spnType = (Spinner) findViewById(R.id.spnType);
        tvFilename = (TextView) findViewById(R.id.tvFilename);
        btnFile = (Button) findViewById(R.id.btnFile);
    }


    /**********************************************************************
     * Purpose: Setup various elements on the EditActivity layout so that
     *          they have event handlers, display the proper text for the
     *          various properties of the sound effect, etc.
     */
    private void setupElements()
    {
        // Get the bundle from the Main Activity
        Bundle bundle = getIntent().getExtras();
        // Get the SoundEffect from the bundle
        sfx = (SoundEffect) bundle.get(MainActivity.SFX);
        // Set up the spinner to display types
        spnType.setAdapter(General.createSpinner(this));

        // If there is a SoundEffect in the bundle
        if (sfx != null)
        {
            // Fill the fields with the appropriate content from the
            //  SoundEffect
            etName.setText(sfx.name);
            etDesc.setText(sfx.description);
            spnType.setSelection(General.getTypeId(this, sfx.type));
            tvFilename.setText(sfx.file);

        } else
        {
            // Make a new SoundEffect
            sfx = new SoundEffect("", "", "", 0, 0, false, "");
        }

        // Set event handlers for the buttons
        btnFile.setOnClickListener(this);

    }


    /**********************************************************************
     * Purpose: This is the event handler for various buttons on this
     *          activity, but there's only one button, which is the file
     *          "Edit" button. So this method handles this button by
     *          calling another method made for that button.
     *          Could be expanded in the future.
     * @param v (View): The view that was clicked on.
     */
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            // If the edit file button was pressed
            case R.id.btnFile:
                // Open a new dialog to handle file choosing
                showFileDialog();
                break;
        }
    }


    /**********************************************************************
     * Purpose: To create the action bar for this activity. It will display
     *          "New Sound" or "Edit Sound" depending on whether we're adding
     *          or editing a sound.
     */
    private void createActionBar()
    {
        // Get the bundle from the Main Activity
        Bundle bundle = getIntent().getExtras();
        // Get the SoundEffect from the bundle
        sfx = (SoundEffect) bundle.get(MainActivity.SFX);

        // Find the toolbar in the activity_main layout
        actionBar = (Toolbar) findViewById(R.id.editToolbar);
        // Set the title bar of the sound effect depending on whether we're
        //  adding or editing a sound effect.
        if (sfx != null)
        {
            actionBar.setTitle(getString(R.string.edit_action_bar_title));
        } else
        {
            actionBar.setTitle(getString(R.string.new_action_bar_title));
        }
        setSupportActionBar(actionBar);
    }


    /**********************************************************************
     * Purpose: To create the options for the action bar for this activity.
     *          Not intended to be used as a regular method.
     * @param menu (Menu): The menu to be inflated.
     * @return A boolean value determining if the menu was successfully
     *         created.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Get a Menu Inflater
        MenuInflater inflater = getMenuInflater();
        // Inflate the menu with the Menu Inflater
        inflater.inflate(R.menu.edit_menu, menu);
        return true;
    }


    /**********************************************************************
     * Purpose: To handle events where a user clicks on one of the action
     *          bar items in the edit activity. Not intended to be used
     *          as a regular method.
     * @param item (MenuItem): The item that was clicked on.
     * @return A boolean value determining if item was handled successfully.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Since there is only one menu item, we only have to worry about
        //  saving, which is being handled by the Main activity
        if (item.getItemId() == R.id.itSave)
        {
            // Handle the saving functionality in another method
            return handleSaveButton();
        } else
        {
            // Let the super class handle the button
            return super.onOptionsItemSelected(item);
        }
    }


    /**********************************************************************
     * Purpose: This method handles the Save button when it's pressed.
     *          It will create a Intent for the sake of returning information
     *          to the MainActivity about the new or edited sound effect
     * @return A boolean value determining if the save button was successfully
     *         handled.
     */
    private boolean handleSaveButton()
    {
        // If all require fields are filled
        if (areRequiredFieldsFilled())
        {
            // Modify the sound effect to reflect the new properties
            saveSound();
            // Create an intent to pass back the sound effect
            Intent intent = new Intent();
            intent.putExtra(MainActivity.SFX, sfx);
            // Set the result
            setResult(RESULT_OK, intent);
            finish();
        }
        else
        {
            // Warn the user that required fields need to be filled
            General.printToast(this, getString(R.string.save_failed));
        }
        return true;
    }


    /**********************************************************************
     * Purpose: This method edits the sound effect that was passed in by the
     *          MainActivity. If the sound effect is new, it edits the new
     *          sound effect made by this activity.
     */
    private void saveSound()
    {
        // Modify the sound effect based on the given input
        sfx.name = etName.getText().toString();
        sfx.description = etDesc.getText().toString();
        sfx.type = spnType.getSelectedItem().toString();
        sfx.file = tvFilename.getText().toString();
    }


    /**********************************************************************
     * Purpose: This method handles the event where the user presses the back
     *          button on their phone. It should cancel any changes made to
     *          the sound being editing by this activity. It should also
     *          cancel the creation of a new sound effect so that it won't
     *          be added to the database.
     */
    @Override
    public void onBackPressed()
    {
        // Cancel the edit/addition
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    /**********************************************************************
     * Purpose: Checks all properties to see if they were filled by the user.
     * @return A boolean value determining if the user filled all properties.
     */
    private boolean areRequiredFieldsFilled()
    {
        return !(etName.getText().toString().equals("") ||
                etDesc.getText().toString().equals("") ||
                tvFilename.getText().toString().equals(""));
    }


    /* File Chooser Dialog-related methods */
    /**********************************************************************
     * Purpose: Loads a list of WAV files into the fileList class variable to
     *          be shown in the File Dialog box.
     */
    private void loadFileList()
    {

        // If this file exists
        if (mPath.exists())
        {
            // Retrieve the list of sound effects using a filter and return it
            FilenameFilter filter = new FilenameFilter()
            {
                @Override
                public boolean accept(File dir, String name) {
                    File select = new File(dir, name);
                    return name.contains(FTYPE) || select.isDirectory();
                }
            };
            fileList = mPath.list(filter);
        }
        else
        {
            // Just return an empty array of strings
            fileList = new String[0];
        }
    }


    /**********************************************************************
     * Purpose: To show a dialog box to the user that gives the user a list
     *          of sound files to choose from.
     */
    private void showFileDialog()
    {
        // A builder for the dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Load a list of files
        loadFileList();

        // Build the dialog with the builder
        builder.setTitle(R.string.filechooser_title);

        // If the list is not blank
        if (fileList != null)
        {
            // Build the list for the dialog
            makeDialogBox( builder );

            // create the dialog and show it
            builder.create();
            builder.show();
        }
    }


    /**********************************************************************
     * Purpose: To add some items to the dialog box showing the user all the
     *          sound files that they can choose from.
     * @param builder (AlertDialog.Builder): The builder trough which we
     *                                       are building the list of items
     *                                       for the dialog box.
     */
    private void makeDialogBox( AlertDialog.Builder builder )
    {
        // Make a list of items in the dialog box showing a list of files
        //  to choose from
        builder.setItems(fileList, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                // When an item is clicked on, update the File field
                //  with the chosen file
                choosenFile = fileList[which];
                tvFilename.setText(choosenFile);
            }
        });
    }
}
