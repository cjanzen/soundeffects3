package com.saskpolytech.cst119.soundeffects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 * Purpose: To provide an interface for communicating with a database that
 * will hold all sound effects for the app. Intended to be used with
 * SoundEffect objects.
 *
 * @author Corey Janzen
 * @version 1.00
 * @created May 20, 2017
 */

public class SoundEffectDatabase extends SQLiteOpenHelper
{

    /* CONSTANTS */
    // The name of the database file
    private static final String DB_NAME = "sounds.db";
    // The name of the table used to store the sound effects
    private static final String TABLE_NAME = "Sounds";
    // The Database version (increment it every time something gets changed)
    private static final int DB_VERSION = 8;
    /**
     * The primary key for the Sounds table
     */
    public static final String ID = "_id";
    /**
     * The name column of the Sounds table
     */
    public static final String NAME = "name";
    /**
     * The description column of the Sounds table
     */
    public static final String DESCRIPTION = "description";
    /**
     * The type column of the Sounds table
     */
    public static final String TYPE = "type";
    /**
     * The latitude column of the Sounds table
     */
    public static final String LATITUDE = "latitude";
    /**
     * The longitude column of the Sounds table
     */
    public static final String LONGITUDE = "longitude";
    /**
     * The shaken column of the Sounds table
     */
    public static final String SHAKEN = "shaken";
    /**
     * The file column of the Sounds table
     */
    public static final String FILE = "file";
    /**
     * The default resource's type (for making default sound effects)
     */
    public static final String R_TYPE = "res/raw";
    /**
     * The default resource's package name (for making default sound
     * effects
     */
    public static final String PACK_NAME =
            "com.saskpolytech.cst119.soundeffects";

    /* Database Queries */
    // Query for making the Sounds table
    private static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (\n" +
                    ID + " integer primary key autoincrement,\n" +
                    NAME + " text not null,\n" +
                    DESCRIPTION + " text,\n" +
                    TYPE + " text not null,\n" +
                    LATITUDE + " real,\n" +
                    LONGITUDE + " real,\n" +
                    SHAKEN + " integer not null,\n" +
                    FILE + " text not null\n" +
                    ");";
    // Query for deleting the Sounds table
    private static final String DELETE_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    /* ATTRIBUTES */
    // A reference to the database for sending commands to
    private SQLiteDatabase sqlDB;

    // A list of default sounds to add to the database
    private DefaultSounds defaultSounds;


    /* CONSTRUCTORS */


    /**********************************************************************
     * Purpose: To construct a SoundEffectDatabase object to be used to
     *          interact with this app's database.
     * @param context (Context): The context to be used for this object
     * @param defaultSounds (DefaultSounds):
     *                      An object that will be used to make default
     *                      sound effects.
     * @return A SoundEffectDatabase object
     */
    public SoundEffectDatabase(Context context, DefaultSounds defaultSounds)
    {
        // Pass the constructing responsibility to the super class
        super(context, DB_NAME, null, DB_VERSION);
        this.defaultSounds = defaultSounds;
    }

    /* BEHAVIOURS */


    /**********************************************************************
     * Purpose: To open the database so that we can read in some content,
     *          as well as write new content, to the database.
     * @throws SQLException
     */
    public void open() throws SQLException
    {
        // Get a writable database
        sqlDB = this.getWritableDatabase();
    }


    /**********************************************************************
     * Purpose: To close the database so that we can free resources
     */
    public void close()
    {
        sqlDB.close();
    }


    /**********************************************************************
     * Purpose: A method that is called when the database doesn't exist when
     *          you try to open it. Not intended to be used as a regular
     *          method.
     * @param db (SQLiteDatabase): A temporary database object used to access
     *                             the database
     */
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        // Create the database if we haven't already
        db.execSQL(CREATE_TABLE);
        // Add some default items
        addDefaultSounds(db);
    }


    /**********************************************************************
     * Purpose: A method that is called when the database version is
     *          incremented. Not intended to be used as a regular method.
     * @param db (SQLiteDatabase): A temporary database object used to access
     *                             the database
     * @param oldVersion (int): The version number of the previous database
     * @param newVersion (int): The version number of the new database
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        // Save all of the entries into a list
        ArrayList<SoundEffect> sfxs = getAllSoundEffects(db);

        // Drop the Sounds table and recreate it with the previously-saved
        //  entries
        db.execSQL(DELETE_TABLE);
        db.execSQL(CREATE_TABLE);

        // Save the entries from the list to the database
        for (SoundEffect s : sfxs)
        {
            createSoundEffect(s, db);
        }
    }


    /**********************************************************************
     * Purpose: To save a SoundEffect object into the database.
     * @param sfx (SoundEffect): The SoundEffect you want to add.
     * @return A long integer containing the id of the SoundEffect.
     */
    public long createSoundEffect(SoundEffect sfx)
    {
        // Create a container to store field values for each row
        ContentValues cvs = getCVS(sfx);

        // Execute the insert statement, which returns the new autoincremented
        //  id
        long autoID = sqlDB.insert(TABLE_NAME, null, cvs);

        // Update the SoundEffect with the new ID
        sfx.id = autoID;
        return autoID;

    }


    /**********************************************************************
     * Purpose: To get a list of sound effects from the database
     * @return An ArrayList object containing all of the SoundEffects from
     *         the database
     */
    public ArrayList<SoundEffect> getAllSoundEffects()
    {
        // Get an array of fields to select
        String[] strFields = new String[]{ID, NAME, DESCRIPTION, TYPE,
                LATITUDE, LONGITUDE, SHAKEN, FILE};
        // Get a Cursor with all of the SoundEffect entries
        Cursor cursor = sqlDB.query(TABLE_NAME, strFields, null, null, null,
                null, null);
        // Turn the cursor into an array of SoundEffects and return that
        return cursorToArrayList(cursor);
    }


    /**********************************************************************
     * Purpose: To translate a cursor containing SoundEffects into an
     *          ArrayList containing SoundEffects
     * @param cursor (Cursor): A cursor containing all of the SoundEffects
     *                         from the database.
     * @return An ArrayList object containing all of the SoundEffects from
     *         the cursor.
     */
    private ArrayList<SoundEffect> cursorToArrayList(Cursor cursor)
    {
        // The array of SoundEffects we will be returning
        ArrayList<SoundEffect> sfxs = new ArrayList<SoundEffect>();

        // While there are entries in the cursor
        while (cursor.moveToNext())
        {
            // Make a SoundEffect object from the entry
            SoundEffect sfx = entryToSoundEffect(cursor);
            // Add the SoundEffect to the ArrayList
            sfxs.add(sfx);
        }

        // Return the ArrayList object
        return sfxs;
    }


    /**********************************************************************
     * Purpose: To translate an entry from a cursor containing a SoundEffect
     *          into a SoundEffect object
     * @param cursor (Cursor): A cursor containing the SoundEffect entry
     * @return A SoundEffect object.
     */
    private SoundEffect entryToSoundEffect(Cursor cursor)
    {
        // A GregorianCalendar object to hold the date
        GregorianCalendar time = new GregorianCalendar();

        // Get all of the attributes from the entry
        long id = cursor.getLong(cursor.getColumnIndex(ID));
        String name = cursor.getString(cursor.getColumnIndex(NAME));
        String description = cursor.getString(cursor.getColumnIndex
                (DESCRIPTION));
        String type = cursor.getString(cursor.getColumnIndex(TYPE));
        double latitude = cursor.getDouble(cursor.getColumnIndex(LATITUDE));
        double longitude = cursor.getDouble(cursor.getColumnIndex(LONGITUDE));
        boolean isShaken = cursor.getInt(cursor.getColumnIndex(SHAKEN)) != 0;
        String file = cursor.getString(cursor.getColumnIndex(FILE));

        // Return a new SoundEffect object with all the attributes from the
        //  entry
        return new SoundEffect(id, name, description, type, latitude,
                longitude, isShaken,
                file);
    }


    /**********************************************************************
     * Purpose: To update a SoundEffect in the database with new values
     * @param sfx (SoundEffect): The SoundEffect that you want to update.
     * @return A boolean value indicating if the specified SoundEffect exists
     *         in the database.
     */
    public boolean updateSoundEffect(SoundEffect sfx)
    {
        // Check to see if this purchase has an id (has been saved)
        if (sfx.id < 0)
        {
            return false;
        } else
        {
            // Create a container to store field values for each row
            ContentValues cvs = getCVS(sfx);

            // Execute the update statement
            return sqlDB.update(TABLE_NAME, cvs, ID + " = " + sfx.id, null) >
                    0;
        }
    }


    /**********************************************************************
     * Purpose: To delete a SoundEffect in the database
     * @param sfx (SoundEffect): The SoundEffect that you want to delete.
     * @return A boolean value indicating if the specified SoundEffect exists
     *         in the database.
     */
    public boolean deleteSoundEffect(SoundEffect sfx)
    {
        // Check to see if this purchase has an id (has been saved)
        if (sfx.id < 0)
        {
            return false;
        } else
        {
            // Delete the SoundEffect from the database
            return sqlDB.delete(TABLE_NAME, ID + " = " + sfx.id, null) > 0;
        }
    }


    /**********************************************************************
     * Purpose: To get an individual SoundEffect from the database with the
     *          specified ID
     * @param id (long): The ID of the SoundEffect you want to retrieve
     * @return A SoundEffect object (null if it doesn't exist)
     */
    public SoundEffect getSoundEffect(long id)
    {
        // The SoundEffect object we will be returning
        SoundEffect sfx = null;

        // Get an array of fields to select
        String[] strFields = new String[]{ID, NAME, DESCRIPTION, TYPE,
                LATITUDE, LONGITUDE, SHAKEN, FILE};
        // Get a Cursor with the specified SoundEffect
        Cursor cursor = sqlDB.query(true, TABLE_NAME, strFields, ID + " = " +
                id, null, null, null, null, null);

        // Retrieve the SoundEffect from the cursor and return it if it exists
        if (cursor.getCount() > 0)
        {
            cursor.moveToNext();
            sfx = entryToSoundEffect(cursor);
        }
        return sfx;
    }


    /**********************************************************************
     * Purpose: To create a ContentValues container to hold values for a
     *          new or updated entry in the database
     * @param sfx (SoundEffect): The SoundEffect that you are putting or
     *                           updating in the database
     * @return A ContentValues object
     */
    private ContentValues getCVS(SoundEffect sfx)
    {
        // Create a container to store field values for each row
        ContentValues cvs = new ContentValues();
        // Add values for each field
        if (sfx.id != -1) // Add the ID if it exists already
        {
            cvs.put(ID, sfx.id);
        }
        cvs.put(NAME, sfx.name);
        cvs.put(DESCRIPTION, sfx.description);
        cvs.put(TYPE, sfx.type);
        cvs.put(LATITUDE, sfx.latitude);
        cvs.put(LONGITUDE, sfx.longitude);
        cvs.put(SHAKEN, sfx.isShaken);
        cvs.put(FILE, sfx.file);

        // Return the ContentValues object
        return cvs;
    }


    /**********************************************************************
     * Purpose: To clear the Sounds table of all data
     */
    public void clear()
    {
        // Drop the Sounds table and recreate it without any entries
        sqlDB.execSQL(DELETE_TABLE);
        sqlDB.execSQL(CREATE_TABLE);
    }


    /**********************************************************************
     * Purpose: To get a list of sound effects from the database filtered by
     *          the specified type
     * @param type (String): The type that you want to filter by
     * @return An ArrayList object containing all of the SoundEffects from
     *         the database of the specified type
     */
    public ArrayList<SoundEffect> getSoundEffectsByFilter(String type)
    {
        // Get an array of fields to select
        String[] strFields = new String[]{ID, NAME, DESCRIPTION, TYPE,
                LATITUDE, LONGITUDE, SHAKEN, FILE};
        // Get a Cursor with all of the SoundEffect entries with the
        //  specified filter
        Cursor cursor = sqlDB.query(true, TABLE_NAME, strFields, TYPE +
                " = '" + type + "'", null, null, null, null, null);
        // Turn the cursor into an array of SoundEffects and return that
        return cursorToArrayList(cursor);
    }


    /**********************************************************************
     * Purpose: To add default sound effects to the database so that the
     *          user can have sound effects to use if they do not have any.
     * @param db (SQLiteDatabase): A temporary database object used to access
     *                             the database
     */
    private void addDefaultSounds( SQLiteDatabase db )
    {
        // The index in the INS array that we're working on
        int i = 0;

        // For each default sound effect
        for ( SoundEffect s : defaultSounds.sfxs )
        {
            // Add it to the database
            createSoundEffect(s, db);
            // Add it to the downloads folder
            addDefaultSoundFile(defaultSounds.ins[i], s);
            i++; // Go to the next entry in the INS entry
        }
    }


    /**********************************************************************
     * Purpose: To add the sound file for a default sound effect to the
     *          "Download" directory of the phone so that we can play
     *          those default sound effects.
     * @param file (InputStream): The file from this app's resources that
     *                            will be created.
     * @param sfx (SoundEffect): The default sound effect we're adding
     */
    private void addDefaultSoundFile(InputStream file, SoundEffect sfx)
    {
        try
        {
            // Try to write the file to the file system
            writeFile( file, sfx );
        }
        catch (FileNotFoundException fnfe)
        {
            // If the file couldn't be found, print a log message about the
            //  missing file
            Log.d("MissingFile", "Couldn't find " + sfx.file + "for some " +
                    "reason.");
        }
        catch (IOException e)
        {
            // If something went wrong during reading, print a log message
            Log.d("IOException", "Something went wrong will reading " +
                    sfx.file);
        }

    }


    /**********************************************************************
     * Purpose: To write the specified sound file to the "Download" directory
     *          of the phone.
     * @param file (InputStream): The file from this app's resources that
     *                            will be created.
     * @param sfx (SoundEffect): The default sound effect we're adding
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void writeFile( InputStream file, SoundEffect sfx )
            throws FileNotFoundException,
            IOException
    {
        // Create an output stream for the file
        FileOutputStream out = new FileOutputStream(SoundEffect.DIRECTORY +
                "/" + sfx.file);
        // Create the buffer
        byte[] buff = new byte[1024];
        // The number of characters to read so for
        int read = 0;

        // While we still need to read characters
        while ((read = file.read(buff)) > 0)
        {
            // Write to the downloads folder
            out.write(buff);
        }

        // Close the file
        file.close();
        out.close();
    }

    /**********************************************************************
     * Purpose: To save a SoundEffect object into the database. This method
     *          is to be used with the onCreate() and onUpgrade() methods,
     *          where sqlDB is null.
     * @param sfx (SoundEffect): The SoundEffect you want to add.
     * @param db (SQLiteDatabase): A temporary database object used to access
     *                             the database
     * @return A long integer containing the id of the SoundEffect.
     */
    private long createSoundEffect(SoundEffect sfx, SQLiteDatabase db)
    {
        // Create a container to store field values for each row
        ContentValues cvs = getCVS(sfx);

        // Execute the insert statement, which returns the new autoincremented
        //  id
        long autoID = db.insert(TABLE_NAME, null, cvs);

        // Update the SoundEffect with the new ID
        sfx.id = autoID;
        return autoID;

    }


    /**********************************************************************
     * Purpose: To get a list of sound effects from the database.This method
     *          is to be used with the onCreate() and onUpgrade() methods,
     *          where sqlDB is null.
     * @param db (SQLiteDatabase): A temporary database object used to access
     *                             the database
     * @return An ArrayList object containing all of the SoundEffects from
     *         the database
     */
    public ArrayList<SoundEffect> getAllSoundEffects( SQLiteDatabase db )
    {
        // Get an array of fields to select
        String[] strFields = new String[]{ID, NAME, DESCRIPTION, TYPE,
                LATITUDE, LONGITUDE, SHAKEN, FILE};
        // Get a Cursor with all of the SoundEffect entries
        Cursor cursor = db.query(TABLE_NAME, strFields, null, null, null,
                null, null);
        // Turn the cursor into an array of SoundEffects and return that
        return cursorToArrayList(cursor);
    }
}
