package com.saskpolytech.cst119.soundeffects;

import android.content.Context;

import java.io.InputStream;

/**
 * Purpose: To provide some default sound effects for the SoundEffectDatabase
 *          when it is first created. Files for these sound effects are found
 *          in the res/raw directory.
 *
 * @author Corey Janzen
 * @version 1.00
 * @created May 28, 2017
 */
public class DefaultSounds
{

    /** The sound effects that the user starts with */
    public SoundEffect[] sfxs;

    /** InputStreams for the above files */
    public InputStream[] ins;

    /**********************************************************************
     * Purpose: To construct a DefaultSound object to be used to make
     *          default sound effects, as well as files for those sounds.
     * @param context (Context): A context object that will be used to get
     *                           string values for the various types of
     *                           sound effects.
     * @return A DefaultSound object with default sounds and InputStreams
     *         for the files associated with those sound effects.
     */
    public DefaultSounds(Context context)
    {
        // Create the sound effects and input streams
        createSFXs( context );
        createInputStreams( context );
    }


    /**********************************************************************
     * Purpose: To create the database entries for the default sound effects.
     * @param context (Context): A context object that will be used to get
     *                           string values for the various types of
     *                           sound effects.
     */
    private void createSFXs( Context context )
    {
        // Create default sound effects for the database
        sfxs = new SoundEffect[]
                {
                        new SoundEffect("Goats",
                                "Goats making noises on the farm.",
                                context.getString(R.string.animals),
                                0,
                                0,
                                false,
                                "goats.wav"),
                        new SoundEffect("Laser Machine Gun",
                                "A rapid-firing laser gun.",
                                context.getString(R.string.scifi),
                                0,
                                0,
                                false,
                                "laser.wav"),
                        new SoundEffect("Rainforest",
                                "The natural, peaceful sounds of the rainforest.",
                                context.getString(R.string.ambience),
                                0,
                                0,
                                false,
                                "rainforest.wav"),
                        new SoundEffect("Ta-Da",
                                "When you have achieved something in life and" +
                                        " want to celebrate it with a song.",
                                context.getString(R.string.musical),
                                0,
                                0,
                                false,
                                "tada.wav"),
                        new SoundEffect("Cartoon Running",
                                "A cartoon character running around.",
                                context.getString(R.string.cartoon),
                                0,
                                0,
                                false,
                                "running_feet.wav"),
                        new SoundEffect("High Five",
                                "Two people giving each other a high five.",
                                context.getString(R.string.others),
                                0,
                                0,
                                false,
                                "slap.wav")
                };
    }


    /**********************************************************************
     * Purpose: To create the sound files for the default sound effects.
     * @param context (Context): A context object that will be used to get
     *                           raw files for the various sound effects.
     */
    private void createInputStreams( Context context )
    {
        // Create the InputStreams to the raw files so that we can access
        //  them.
        ins = new InputStream[]
                {
                        context.getResources().openRawResource(R.raw.goats),
                        context.getResources().openRawResource(R.raw.laser),
                        context.getResources().openRawResource(R.raw.rainforest),
                        context.getResources().openRawResource(R.raw.tada),
                        context.getResources().openRawResource(R.raw.running_feet),
                        context.getResources().openRawResource(R.raw.slap)
                };
    }
}
