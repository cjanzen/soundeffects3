package com.saskpolytech.cst119.soundeffects;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Purpose: To provide an interface through which the ListView can be updated
 * with Sound Effects from a database.
 *
 * @author Corey Janzen
 * @version 1.00
 * @created May 21, 2017
 */
public class SoundListAdapter extends ArrayAdapter<SoundEffect>
{
    /* CONSTANTS */
    /** The text for the various TextViews to be displayed before the value */
    public static final String LATITUDE = "Latitude: ";
    public static final String LONGITUDE = "Longitude: ";


    /* ATTRIBUTES */
    // A reference to the MainActivity
    private MainActivity main;


    /* BEHAVIOURS */


    /**********************************************************************
     * Purpose: To construct a SoundListAdapter object to be used to
     *          build the ListView in the MainActivity.
     * @param context (Context): The context to be used for this object
     * @param sounds (ArrayList): The list of sound effects to be used with
     *                            with this adapter.
     * @param main (MainActivity): A reference to the MainActivity
     * @return A SoundListAdapter object
     */
    public SoundListAdapter(Context context, ArrayList<SoundEffect> sounds,
                            MainActivity main)
    {
        super(context, R.layout.sound_list_item, sounds);
        this.main = main;
    }


    /**********************************************************************
     * Purpose: To get a View object for a specific item in the list
     *          Not intended to be used as a regular method.
     * @param position (int): The current row index of the ListView.
     * @param convertView (View): the View to be converted.
     * @param parent (ViewGroup): The parent ViewGroup of this View
     * @return A View object representing this View.
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull
            ViewGroup parent)
    {
        // Get the sound effect from the required position
        SoundEffect sfx = this.getItem(position);

        // Reuse an old row if possible
        View soundItemView = convertView;
        if (convertView == null)
        {
            // Create a visual representation of the row defined by our xml
            LayoutInflater inflater = (LayoutInflater) this.getContext()
                    .getSystemService(Context
                            .LAYOUT_INFLATER_SERVICE);
            soundItemView = inflater.inflate(R.layout.sound_list_item,
                    parent, false);
        }

        // Set up the controls for each item
        setupControls(soundItemView, sfx, position);

        return soundItemView;
    }


    /**********************************************************************
     * Purpose: To setup controls for the specified view
     * @param soundItemView (View): the View representing a List item.
     * @param sfx (SoundEffect): The SoundEffect That the above view is
     *                           representing
     * @param position (int): The current row index of the ListView.
     */
    private void setupControls( View soundItemView, SoundEffect sfx,
                                int position)
    {
        // Get references to the controls on the current row of the ListView
        TextView tvSName = (TextView) soundItemView.findViewById(R.id.tvSName);
        TextView tvSType = (TextView) soundItemView.findViewById(R.id.tvSType);
        TextView tvSLat = (TextView) soundItemView.findViewById(R.id.tvSLat);
        TextView tvSLon = (TextView) soundItemView.findViewById(R.id.tvSLon);
        GridLayout layout = (GridLayout) soundItemView.findViewById(R.id
                .glSoundItem);
        Button btnEdit = (Button) soundItemView.findViewById(R.id.btnEdit);
        Button btnDelete = (Button) soundItemView.findViewById(R.id.btnDelete);

        // Add the data to the TextViews and event handlers for the buttons.
        tvSName.setText(sfx.name);
        tvSType.setText(sfx.type);
        tvSLat.setText(LATITUDE + sfx.latitude);
        tvSLon.setText(LONGITUDE + sfx.longitude);
        layout.setOnClickListener(new ButtonHandler(position));
        btnEdit.setOnClickListener(new ButtonHandler(position));
        btnDelete.setOnClickListener(new ButtonHandler(position));
        soundItemView.requestFocus();
    }



    /**
     * Purpose: To provide a handler for each button/layout in the ListView
     *          that is associated with this adapter.
     *
     * @author Corey Janzen
     * @version 1.00
     * @created May 21, 2017
     */
    class ButtonHandler implements View.OnClickListener
    {
        private int position;

        public ButtonHandler( int position )
        {
            this.position = position;
        }

        @Override
        /**********************************************************************
         * Purpose: Handles events where the user clicks on an item in the list
         *          or one of the item's buttons.
         *          Not intended to be used as a regular method.
         */
        public void onClick(View v)
        {
            // Get the SoundEffect for this row
            SoundEffect sfx = getItem(position);
            // Handle the functionality in the main activity
            main.handleButtons(sfx, v);
        }



    }
}
