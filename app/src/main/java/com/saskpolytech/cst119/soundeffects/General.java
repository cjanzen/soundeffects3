package com.saskpolytech.cst119.soundeffects;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Purpose: To provide various general-purpose methods that can be used in
 *          many classes.
 *
 * @author Corey Janzen
 * @version 1.00
 * @created May 21, 2017
 */

public class General
{



    /**********************************************************************
     * Purpose: To print a toast onto the screen.
     * @param context (Context): The context that this toast will be printed
     *                           on.
     * @param msg (String): The message you want to print out.
     */
    public static void printToast(Context context, String msg)
    {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }


    /**********************************************************************
     * Purpose: To make an ArrayAdapter containing Strings of the various
     *          types of sound effects so that it can be attached to a
     *          Spinner.
     * @param context (Context): The context that will be used to get string
     *                           values from the strings.xml file.
     * @return An ArrayAdapter containing Strings to be attached to a Spinner.
     */
    public static ArrayAdapter<String> createSpinner(Context context)
    {
        // Create an array
        ArrayList<String> strs = new ArrayList<String>();
        // Add all sound effect types into this array
        strs.add(context.getResources().getString(R.string.ambience));
        strs.add(context.getResources().getString(R.string.animals));
        strs.add(context.getResources().getString(R.string.cartoon));
        strs.add(context.getResources().getString(R.string.musical));
        strs.add(context.getResources().getString(R.string.scifi));
        strs.add(context.getResources().getString(R.string.others));

        // Put this array into an adapter to return the adapter
        return new ArrayAdapter<String>(context, android.R.layout
                .simple_list_item_1, strs);
    }


    /**********************************************************************
     * Purpose: To get the id of a type of sound effect to be used with the
     *          Spinner in the EditActivity so that we can get the appropriate
     *          entry selected in that Spinner.
     * @param context (Context): The context that will be used to get string
     *                           values from the strings.xml file.
     * @param type (String): The type of sound effect that you're trying to
     *                       get the id for.
     * @return An integer value representing an entry of the Spinner view in
     *         the EditActivity.
     */
    public static int getTypeId( Context context, String type )
    {
        // Find the appropriate id for the type and return it
        return getTypeHash(context).get(type);
    }


    /**********************************************************************
     * Purpose: A helper method of getTypeId() that generates a list of the
     *          types of sound effects and their associated ids
     * @param context (Context): The context that will be used to get string
     *                           values from the strings.xml file.
     * @return A HashMap containing a list of the types of sound effects
     *         and their associated ids.
     */
    private static HashMap<String, Integer> getTypeHash(Context context)
    {
        // Create a HashMap
        HashMap<String, Integer> types = new HashMap<String,Integer>();
        // Add all sound effect types into this array
        types.put(context.getResources().getString(R.string.ambience), 0);
        types.put(context.getResources().getString(R.string.animals), 1);
        types.put(context.getResources().getString(R.string.cartoon), 2);
        types.put(context.getResources().getString(R.string.musical), 3);
        types.put(context.getResources().getString(R.string.scifi), 4);
        types.put(context.getResources().getString(R.string.others), 5);

        // Return the hashmap
        return types;
    }
}
