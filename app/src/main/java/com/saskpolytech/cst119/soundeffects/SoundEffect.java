package com.saskpolytech.cst119.soundeffects;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/**
 * Purpose: To represent a sound effect and it's various attributes, such as
 * its type, location it was last used, a reference to the sound
 * file, etc. Intended to be used in representing an entry from
 * a database that holds a list of various sound effects.
 *
 * @author Corey Janzen
 * @version 1.00
 * @created May 17, 2017
 */

public class SoundEffect implements Serializable
{

    /* CONSTANTS */
    // The Sound effect's default type
    private static final int DEFAULT_TYPE = R.string.others;
    // The default ID for a sound effect when the sound effect doesn't have
    // one
    private static final long DEFAULT_ID = -1;
    /**
     * The location for all of the sound effects
     */
    public static final File DIRECTORY = Environment
            .getExternalStoragePublicDirectory(Environment
                    .DIRECTORY_DOWNLOADS);
    /** The semantic time */
    public static final GregorianCalendar SEMANTIC_TIME = new GregorianCalendar();

    /**
     * The format that the time will be in
     */
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat
            ("h:mm a");

    /* ATTRIBUTES */
    /**
     * The sound effect's ID. Stored as an long integer
     */
    public long id;
    /**
     * The sound effect's name. Stored as a String
     */
    public String name;
    /**
     * The sound effect's description. Stored as a String
     */
    public String description;
    /**
     * The sound effect's type. Stored as a String
     */
    public String type;
    /**
     * The last location where the sound effect was played. Stored as
     * real values
     */
    public double latitude;
    public double longitude;
    /**
     * The scheduled time for the sound effect. Stored as a Date object
     */
    //public GregorianCalendar time;
    /**
     * The indicator that determines if the sound effect should be played
     * when shaken or not. Stored as a boolean.
     */
    public boolean isShaken;
    /**
     * The reference to the file that the sound effect is stored. Stored
     * as a String
     */
    public String file;


    /* CONSTRUCTORS */


    /**********************************************************************
     * Purpose: To construct a SoundEffect object to be used to represent
     *          sound effects.
     * @param id (long): The id for this sound effect.
     * @param name (String): The name for this sound effect.
     * @param description (String): The description for this sound effect.
     * @param type (String): The sound effect's type.
     * @param latitude (double): The sound effect's latitude location.
     * @param longitude (double): The sound effect's longitude location.
     * @param isShaken (isShaken): Whether or no this sound effect is trigger
     *                             via shaking.
     * @param file (String): A string reference to the file holding this
     *                       sound effect.
     * @return A SoundEffect object
     */
    // Fully Loaded constructor.
    public SoundEffect(long id, String name, String description, String type,
                       double latitude, double longitude, boolean isShaken,
                       String file)
    {
        // Initialize variables
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isShaken = isShaken;
        this.file = file;
    }


    /**********************************************************************
     * Purpose: To construct a SoundEffect object to be used to represent
     *          sound effects.
     * @param name (String): The name for this sound effect.
     * @param description (String): The description for this sound effect.
     * @param type (String): The sound effect's type.
     * @param latitude (double): The sound effect's latitude location.
     * @param longitude (double): The sound effect's longitude location.
     * @param isShaken (isShaken): Whether or no this sound effect is trigger
     *                             via shaking.
     * @param file (String): A string reference to the file holding this
     *                       sound effect.
     * @return A SoundEffect object
     */
    public SoundEffect(String name, String description, String type,
                       double latitude, double longitude, boolean isShaken,
                       String file)
    {
        // Initialize variables
        this(DEFAULT_ID, name, description, type, latitude, longitude,
                isShaken, file);
    }

    /* BEHAVIOURS */


    /**********************************************************************
     * Purpose: To make a MediaPlayer object to play this sound effect.
     * @param context (Context): The context to be used for the MediaPlayer
     *                           object.
     * @return A MediaPlayer object
     */
    public MediaPlayer makeMediaPlayer(Context context)
    {
        // Get a reference to the file holding the sound effect
        File soundFile = new File(DIRECTORY, file);
        // Transform the file reference into a URI
        Uri soundUri = Uri.fromFile(soundFile);

        // Use the URI to make a new MediaPlayer object
        return MediaPlayer.create(context, soundUri);
    }


    /**********************************************************************
     * Purpose: To determine if one SoundEffect is equal to another
     * @param s (SoundEffect): The SoundEffect you want to compare with
     * @return A boolean value indicating if this SoundEffect matches the
     *         specified SoundEffect.
     */
    public boolean equals(SoundEffect s)
    {
        return this.id == s.id &&
                this.name.equals(s.name) &&
                this.description.equals(s.description) &&
                this.type.equals(s.type) &&
                this.latitude == s.latitude &&
                this.longitude == s.longitude &&
                this.isShaken == s.isShaken &&
                this.file.equals(s.file);
    }

}
