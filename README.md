# SoundEffects

A mobile application for Android that I developed using the Android Studio IDE for the Mobile Application class. This app allows you to play sound effects on your phone. You can play any of the builtin sound effects, or you can add custom sound effects from the Downloads folder of your phone and play those as well so long as they're WAV files. Every time you play a sound effect, the GPS coordinates for that sound effect will be updated to show where you last played it.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites

- **Android Studio**: The IDE that was used to develop the app. You can download it from https://developer.android.com/studio/.

### Installing
*Note*: You can download a [pre-built APK](https://bitbucket.org/cjanzen/soundeffects3/downloads/soundeffects.apk) that you can install onto your Android device if you don't want to build the APK yourself. You may need to enable installations from Unknown Sources by tapping "Settings > Security" and checking "Unknown Sources" prior to installing the APK.

Ensure that you have installed Android Studio onto your machine. Once you have done so, you can open the project in Android Studio. You may need to install some packages/dependencies prior to being able to run the project. They should be shown at the bottom of the IDE window as links that you can click on to install said packages/dependencies.

Once you have installed the packages/dependencies, you should be able to run the application using an emulator or a physical Android device.

### Usage

I have left a Microsoft Word document at the root of the project called "User Manual.docx" that explains how to use the application.

## Built With

* [Android Studio](https://developer.android.com/studio/) - The IDE used to develop the app.

## Authors

* **Corey Janzen**  - [cjanzen](https://bitbucket.org/cjanzen/)

## Acknowledgments

* All sound effects were retrieved from http://soundbible.com/.
	* [**Cartoon Running**](http://soundbible.com/1979-Cartoon-Running.html): Cam
	* [**Ta-Da**](http://soundbible.com/1003-Ta-Da.html): Mike Koenig
	* [**Laser Machine Gun Sound**](http://soundbible.com/1774-Laser-Machine-Gun.html): Mike Koenig

> Written with [StackEdit](https://stackedit.io/).